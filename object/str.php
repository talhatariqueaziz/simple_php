<?php
	//String obj
	class STR{
		//obj property
		public $process_str;

		//constructor
		public function __construct(){
			$this->process_str = "";
		}

		//set input string
		function setSTR($someString){
			$this->process_str = $someString;
			$this->sanitizer();
		}

		//sanitizer - to strip special chars
		function sanitizer(){
			$this->process_str = htmlspecialchars(strip_tags($this->process_str));
		}
		
		//turn string to uppercase
		function upperCASE(){
			$this->process_str = strtoupper($this->process_str);
		}

		//turn string to upper & lower case combination
		function upperLowerCombination(){
			$str_array = str_split($this->process_str); //convert string into array
			$this->process_str = null;
			$converter = false; //indicator to change between upper & lower

			foreach($str_array as $e){ //accessing individual string char
				if(!$converter){
					$e = strtolower($e);
					$converter = true;
				}else{
					$e = strtoupper($e);
					$converter = false;
				}
				$this->process_str .= $e;
			}

		}

		//conver string into csv file
		function stringToFile(){
			$str_array = str_split($this->process_str); //convert string into array
			$file = fopen("string.csv","w"); //open file to write

			foreach ($str_array as $e){ //accessing individual char to write to file
				fputcsv($file, array($e));
			}
			//output file into main directory
			fclose($file);

			//output requested line
			echo "CSV created!\r\n";
		}
	}
?>
