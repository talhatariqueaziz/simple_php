<?php
	//include String obj file
	include_once "./object/str.php";
	
	//initialize String obj
	$customString = new STR();
	

	//initialize variables
	$line = null;
	$output = null;

	do{
		echo "Enter a string: ";
		$handle = fopen("php://stdin", "r");
		$line = fgets($handle);
		if(strlen($line) == 1)
			$line = null;
	}while(empty($line));


	$customString->setSTR($line); //set input to obj attribute
	
	//convert string all caps
	$customString->upperCase();
	echo "\r\n\r\nSample Ouput: \r\n\r\n" . $customString->process_str; //display all caps


	//convert string all caps and lower
	$customString->upperlowerCombination();
	echo $customString->process_str;
	

	//create CSV file within same folder
	$customString->setSTR($line); //reset input to obj attribute
	$customString->stringToFile();
?>
